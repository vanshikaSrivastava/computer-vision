/*
   Video to Images: Write a program to convert a given video to its constituent images. Your output should be
   in a specified folder. Write another program that will merge a set of images in a folder into a single video. You
   should be able to control the frame rate in the video that is created.
   */
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <iostream>

using namespace cv;
using namespace std;

int main (int argc, char** argv)
{
	// Initialize capture from file
	CvCapture * capture = cvCaptureFromAVI ("1.mov");

	// Capturing the first frame
	IplImage* img = 0;
	if(!cvGrabFrame(capture))      //capture a frame
	{
		printf("Could not grab a frame\n");
		exit(0);
	}

	// find out the number of frames
	double num_frames = cvGetCaptureProperty (capture, CV_CAP_PROP_FRAME_COUNT);

	char name[100];	// for filename

	// capture frame by frame
	for (int i = 0; i < (int)num_frames; i++)
	{
		img=cvRetrieveFrame(capture);    //retrieve the captured frame
		sprintf(name,"frames/%d.jpg", i); 
		cvSaveImage(name, img);
		cvGrabFrame(capture);
	}

	cvReleaseCapture(&capture);

	return 0;

}
