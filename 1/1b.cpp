/*
   Images ↔ Video: Write another program that will merge a set of images in a folder into a single video. You
   should be able to control the frame rate in the video that is created.
   */
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include<opencv/cvaux.h>
#include<opencv/cxcore.h>

using namespace cv;
using namespace std;



int main()
{
	IplImage* img = 0; 

	double dWidth = 1280.00;
	double dHeight = 720.00;

	Size frameSize(static_cast<int>(dWidth), static_cast<int>(dHeight));

	// initialize the VideoWriter object 
	VideoWriter oVideoWriter ("1b_video.avi", CV_FOURCC('P','I','M','1'), 20, frameSize, true);

	if ( !oVideoWriter.isOpened() ) //if the VideoWriter is not initialized successfully, exit the program
	{
		cout << "ERROR: Failed to write the video" << endl;
		return -1;
	}

	char name[100];	// for filename

	for(int fileNo;fileNo<100;fileNo++)
	{	
		sprintf(name,"frames/%d.jpg", fileNo); 
		
		img=cvLoadImage(name);
		oVideoWriter.write(img); // write the frame into the file

	}

	oVideoWriter.release();

	return 0;
}
