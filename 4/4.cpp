/*

   Extract 1st frame
   Get next frame
   compute the histogram correlation
   if correlation < 0.5
   	change shot
   */

#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include<opencv/cvaux.h>
#include<opencv/cxcore.h>



using namespace cv;
using namespace std;

int main (int argc, char** argv)
{
	// Initialize capture from file
	CvCapture * capture = cvCaptureFromAVI ("4.mp4");

	// Capturing the first frame
	Mat current_frame;
	Mat next_frame;
	if(!cvGrabFrame(capture))      //capture a frame
	{
		printf("Could not grab a frame\n");
		exit(0);
	}

	// find out the number of frames
	double num_frames = cvGetCaptureProperty (capture, CV_CAP_PROP_FRAME_COUNT);

	char name[100];	// for filename
	float r_ranges[] = { 0, 256 };
	float g_ranges[] = { 0, 256 };

	const float* ranges[] = { r_ranges, g_ranges };
	MatND hist_base;
	MatND hist_base1;

	int channels[] = {0 , 1,2};
	int r_bins = 50; 
	int g_bins = 50;
	int histSize[] = { r_bins, g_bins};
	// capture frame by frame

	current_frame=cvRetrieveFrame(capture);    //retrieve the captured frame 
	Size frameSize = current_frame.size();
	VideoWriter oVideoWriter ("0.avi", CV_FOURCC('P','I','M','1'), 20, frameSize, true);	

	// capture frame by frame
	int i = 0;
	int count = 0;
	VideoWriter what;
	what = oVideoWriter;
	what.write(current_frame); // write the frame into the file
	for (int i = 0; i < (int)(num_frames-1.00); i++)
	{
		cvGrabFrame(capture);
		next_frame=cvRetrieveFrame(capture);    //retrieve the captured frame

		calcHist( &current_frame, 1, channels, Mat(), hist_base, 2, histSize, ranges, true, false );
		normalize( hist_base, hist_base, 0, 1, NORM_MINMAX, -1, Mat() );
		calcHist( &next_frame, 1, channels, Mat(), hist_base1, 2, histSize, ranges, true, false );
		normalize( hist_base1, hist_base1, 0, 1, NORM_MINMAX, -1, Mat() );
		
		int compare_method = 0;
		double base_half = compareHist( hist_base, hist_base1, compare_method );
		if(base_half < 0.5)
		{
			printf( "%f \n", base_half );
			count++;
			sprintf(name,"%d.avi", count); 
			oVideoWriter.release();
			VideoWriter oVideoWriter (name, CV_FOURCC('P','I','M','1'), 20, frameSize, true);	
			if ( !oVideoWriter.isOpened() ) //if the VideoWriter is not initialized successfully, exit the program
			{
				cout << "ERROR: Failed to write the video" << endl;
				return -1;
			}
			what = oVideoWriter;



		}
		what.write(next_frame); // write the frame into the file
		current_frame = next_frame.clone();
	
	}

	cvReleaseCapture(&capture);
	oVideoWriter.release();
	return 0;

}
