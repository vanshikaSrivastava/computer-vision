#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>
#include <stdio.h>
#include <fstream>

using namespace std;
using namespace cv;

String face_cascade_name = "lbpcascade_frontalface.xml";
CascadeClassifier face_cascade;

int main( int argc, const char** argv )
{
	if( !face_cascade.load( face_cascade_name ))
	{
		printf("(!)Error loading\n");
		return -1;
	};

	ofstream file;
	file.open("output5/Output.txt");

	int j;
	char path[150];
	char path_out[150];
	Mat img;
	for (j=1;j<41;j++)
	{
		sprintf(path, "Images/image_%d.jpg", j );
	
		img = imread(path);
		if(!img.empty())
		{
			Mat frame = img;
			file << "image_" << j<< ".jpg"<<endl;

			std::vector<Rect> faces;
			Mat frame_gray;

			cvtColor( frame, frame_gray, CV_BGR2GRAY );
			equalizeHist( frame_gray, frame_gray );


			face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, 0, Size(10, 100) );

			file << faces.size() << endl;
			for( int i = 0; i < faces.size(); i++ )
			{
				Point pt1(faces[i].x, faces[i].y);
				Point pt2(faces[i].x + faces[i].width, faces[i].y + faces[i].height);
				file << faces[i].x<<" "<< faces[i].y<<" "<< faces[i].width<<" "<<faces[i].height<<endl;
				rectangle(frame, pt1, pt2, Scalar(0,255,0), 3);

			}
			sprintf(path_out, "output5/image_%d.jpg", j);
			imwrite(path_out, frame);
	
		}
		else
			cout << "Image empty" << endl;
	}
	return 0;
}


