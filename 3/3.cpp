#include <stdlib.h>
#include <iostream>
#include<cstring>
#include <stdio.h>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <opencv/cv.h>
#include <opencv/highgui.h>



using namespace cv;

void chromakey(const Mat under, const Mat over, Mat *dst, const Scalar& color) {
	if(under.rows != over.rows || under.cols != over.cols) {
		std::cout << "Error, image dimensions must match" << std::endl;
		return;
	}
	*dst = Mat(under.rows, under.cols, CV_8UC3, CV_RGB(0,0,0));
	for(int y=0; y<under.rows; y++) {
		for(int x=0; x<under.cols; x++) {
			if(((over.at<Vec3b>(y,x)[0]-color[0])*(over.at<Vec3b>(y,x)[0]-color[0])) + ((over.at<Vec3b>(y,x)[1] - color[1])*(over.at<Vec3b>(y,x)[1] - color[1]))+ ((over.at<Vec3b>(y,x)[2]-color[2])*(over.at<Vec3b>(y,x)[2]-color[2])) <= 20000) {
				dst->at<Vec3b>(y,x)[0] = under.at<Vec3b>(y,x)[0];
				dst->at<Vec3b>(y,x)[1] = under.at<Vec3b>(y,x)[1];
				dst->at<Vec3b>(y,x)[2] = under.at<Vec3b>(y,x)[2];
			} else {
				dst->at<Vec3b>(y,x)[0] = over.at<Vec3b>(y,x)[0];
				dst->at<Vec3b>(y,x)[1] = over.at<Vec3b>(y,x)[1];
				dst->at<Vec3b>(y,x)[2] = over.at<Vec3b>(y,x)[2];
			}
		}
	}
}

using namespace std;
int main() 
{
	const char* window_name = "Hough Line Detection";
	namedWindow("MyVideo",CV_WINDOW_AUTOSIZE); //create a window to display the video
	Mat dst;
	// Initialize capture from file
	CvCapture * uv = cvCaptureFromAVI ("under.mp4");
	CvCapture * ov = cvCaptureFromAVI ("over2.mp4");

	// Capturing the first frame
	Mat over;
	Mat under;
	if(!cvGrabFrame(uv))      //capture a frame
	{
		printf("Could not grab a frame\n");
		exit(0);
	}

	if(!cvGrabFrame(ov))      //capture a frame
	{
		printf("Could not grab a frame\n");
		exit(0);
	}
	// find out the number of frames
	double num_frames_uv = cvGetCaptureProperty (uv, CV_CAP_PROP_FRAME_COUNT);
	double num_frames_ov = cvGetCaptureProperty (ov, CV_CAP_PROP_FRAME_COUNT);
	double num_frames;
	if(num_frames_uv > num_frames_ov)
		num_frames = num_frames_ov;
	else
		num_frames = num_frames_uv;

	over=cvRetrieveFrame(ov);    //retrieve the captured frame
	Size frameSize = over.size();

	// initialize the VideoWriter object 
	VideoWriter oVideoWriter ("1b_video.avi", CV_FOURCC('P','I','M','1'), 20, frameSize, true);

	if ( !oVideoWriter.isOpened() ) //if the VideoWriter is not initialized successfully, exit the program
	{
		cout << "ERROR: Failed to write the video" << endl;
		return -1;
	}


	// capture frame by frame
	for (int i = 0; i < (int)num_frames; i++)
	{
		under=cvRetrieveFrame(uv);    //retrieve the captured frame
		chromakey(under, over, &dst, Scalar(1, 255, 0));
		imshow("MyVideo",dst); //show the frame in the same video
		if (waitKey(10) == 27) // if 'esc' key is pressed, break
		{
			cout << "Exit" << endl;
			break; 
		}
		oVideoWriter.write(dst); // write the frame into the file
		cvGrabFrame(uv);
		cvGrabFrame(ov);
		over=cvRetrieveFrame(ov);    //retrieve the captured frame
	}
	cvReleaseCapture(&ov);
	cvReleaseCapture(&uv);
	oVideoWriter.release();


	return 0;
}

