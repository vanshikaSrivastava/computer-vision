#include "opencv2/highgui/highgui.hpp"
#include<cstring>
#include<stdio.h>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char* argv[])
{
	VideoCapture cap(0); // open the default video camera

	if (!cap.isOpened())  // exit program in case of an error
	{
		cout << "ERROR: Cannot open the video file" << endl;
		return -1;
	}

	namedWindow("MyVideo",CV_WINDOW_AUTOSIZE); //create a window to display the video

	
	int counter = 0;
	char name[20] = "";
	while (1)
	{

		Mat frame;
		bool bSuccess = cap.read(frame); // read a new frame
		if (!bSuccess) // break loop in case of an error
		{
			cout << "ERROR: Cannot read a frame from video file" << endl;
			break;
		}

		imshow("MyVideo", frame); //show the frame in the same video

		if (waitKey(10) == 32) // if spacebar is hit 'capture'
		{
			sprintf(name,"%d.jpg",counter++);
			imwrite(name,frame);
			cout << "Captured" << endl;
		}

		else if (waitKey(10) == 27) // if 'esc' key is pressed, break
		{
			cout << "Exit" << endl;
			break; 
		}
	}

	return 0;

}
